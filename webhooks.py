#!/usr/bin/python
"""
    webhook
"""

from flask import Flask
from flask import request
from flask import json
from datetime import datetime, timedelta
from functools import wraps
from ipaddress import IPv4Network, IPv4Address
import subprocess

class Throttle(object):
    def __init__(self, seconds=0, minutes=0, hours=0):
        self.throttle_period = timedelta(
            seconds=seconds, minutes=minutes, hours=hours
        )
        self.time_of_last_call = datetime.min

    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            now = datetime.now()
            time_since_last_call = now - self.time_of_last_call
            if time_since_last_call > self.throttle_period:
                self.time_of_last_call = now
                return fn(*args, **kwargs)
        return wrapper

config = json.loads(open('config.json', 'r').read())
listen = {
    'port': config[u'global'].get(u'port', 5000),
    'host': config[u'global'].get(u'listen', '127.0.0.1')
}
app = Flask(__name__)
app.debug = config[u'global'][u'debug']

@Throttle(seconds=20)
def run(sh):
    subprocess.check_call(sh, shell=True)


@app.route('/w/<project_name>', methods=['POST'])
def wh(project_name):
    project = config[u'projects'].get(project_name, -1)
    access = False

    if project == -1:
        raise Exception("Project not found")

    white_list = [IPv4Network(x) for x in project.get(u'white_list', config[u'global'][u'white_list'])]
    remote_address = IPv4Address(request.remote_addr.decode('utf8'))

    access = reduce(lambda x, y: x or (remote_address in y), white_list, False)
    
    if not access:
        raise Exception("Forbidden")

    hook = json.loads(request.data)
    repo = hook[u'repository'][u'full_name'] == project[u'full_name']

    if not repo:
        raise Exception(u"Wrong repository for %s" % project_name)

    run_job = False
    for ch in hook[u'push'].get(u'changes', []):
        if ch[u'new'][u'type'] == 'branch' and ch[u'new'][u'name'] == project[u'branch']:
            run_job = True
    if run_job:
        run(project[u'exec'])

    return '1'

if __name__ == '__main__':
    app.run(**listen)
