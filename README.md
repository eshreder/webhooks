# Мини обработчик вэб-хуков на python #
## Зависимости ##

* [flask](http://flask.pocoo.org/)
* [ipaddress](https://docs.python.org/3/library/ipaddress.html)


```
pip install flask
pii install ipaddress
```


## Настройка  ##

В **config.json** 90% всех настроек.

Пример для этого проекта:

```
#!javascript
//config.json

{
 "global": ...,
 "projects": {
   "webhook": {
    "full_name": "eshreder/webhook",
    "branch": "masterk",
    "exec": "./job/updatemwh.sh"
   }
 }
}
```

```
#!shell
#./job/updatemwh.sh

cd /root/path/project
git pull origin master
#bower install
#gulp build
```

Как доставлять трафик до ./webhooks.py, с какими правами работать и так далее - решать по ситуации. Сейчас проксирую все через nginx на localhost, ./webhooks.py работает с правами отдельного пользователя 

Сервер будет ждать запросы (**webhook_url**) на : //host//w/**webhook**
